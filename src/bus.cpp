#include "include/bus.h"

Bus::Bus(Sim *parent_sim){
  this->parent = parent_sim;
}

int Bus::rd(int proc, address_t block){
  //Inform all processors except the caller that this op has happened
  int index = block.index;
  if ( this->parent->output ){ INFO("broadcasting bus_rd from p" << proc << " for block: " <<
				    index); }
  for ( int i = 0; i < 4; ++i ){
    if ( i == proc ){ continue; } //Skip the caller
    this->parent->cpu_array[i]->bus_rd_callback(block);
  }

  return 0;
}

Sim *Bus::show_parent() { return this->parent; }

int Bus::rdx(int proc, address_t block) {
  //Inform all processors except the caller that this op has happened
  int index = block.index;
  if ( this->parent->output ){ INFO("broadcasting bus_rdx from p" << proc << " for block: " <<
				    index); }
  for ( int i = 0; i < 4; ++i ){
    if ( i == proc ){ continue; }
    this->parent->cpu_array[i]->bus_rdx_callback(block);
  }

  return 0;
}

int Bus::upgr(int proc, address_t block) {
  //Inform all processors except the caller that this op has happened
  int index = block.index;
  
  if ( this->parent->output ){ INFO("broadcasting bus_upgr from p" << proc << " for block: " <<
				    index); }
  for ( int i = 0; i < 4; ++i ){
    if ( i == proc ){ continue; }
    this->parent->cpu_array[i]->bus_upgr_callback(block);
  }

  return 0;
}

bool Bus::exc_check(int proc, address_t block){
  //Exclusivity check: return true if block is exclusive, false if not exclusive
  for ( int i = 0; i < 4; ++i ){
    if ( i == proc ){ continue; }
    if ( this->parent->cpu_array[i]->bus_res_request(block) ){ return false; }
  }
  return true; //
}

int Bus::select_server(address_t block){
  //Pick the least id which contains the block
  //Due to order of events, the requesting processor does not yet
  //contain the block so we can't end up with the processor supplying
  //itself with the block

  for ( int i = 0; i < 4; ++i ){
      if ( this->parent->cpu_array[i]->bus_res_request(block) ){ return i; }
  }

  return -1; // Control value
}

int Bus::write_update(int proc, address_t block){
  //for each processor which isn't the caller
  //If block is resident in their cache; update it
  for ( int i = 0; i < 4; ++i ){
    if ( i == proc ){ continue; }
    //    if ( this->parent->cpu_array[i]->bus_res_request(block) ){
      this->parent->cpu_array[i]->bus_write_update(block); //Send the write update
      //    }
  }
 
  return 0;
}
