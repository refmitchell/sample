#include "include/global.h"
#include "include/sim.h"

int main( int argc, char *argv[] ) {

  std::string file_name = "";
  std::string protocol = "MSI";
  int line_size = 4;
  std::string line;
  std::ifstream infile;
  Sim *sim;


  if ( !((argc == 2) || (argc == 3) || (argc == 4)) ){
    ERR("sim: err: incorrect number of arguments");
    LOG("sim: info: usage: $: ./cache_sim trace_file.txt [line_size argument] [coherency protocol]");
    exit (1);
  } else if ( argc == 3 ){ //Greater than 2 and legal, must be 3 or 4
    // Have one optional arg
    int val;
    std::istringstream iss(argv[2]);
    if ( !(iss >> val) ){ //Then it's not an int and not the line-size parameter
      protocol = std::string(argv[2]);
    } else { //If the stream was successful then the value was an integer
      line_size = val;
    }
  } else if ( argc == 4 ){
    // Have two optional args
    int val;
    std::istringstream iss(argv[2]);
    std::istringstream iss2(argv[3]);
    
    if ( !(iss >> val) ){ //Check first opt arg
      protocol = std::string(argv[2]); //wasnt an int so assume string
      if ( !(iss2 >> val) ){ //Check if the second was an int -> if not exit
	ERR("sim: err: two arguments given, expected one string and one integer");
	LOG("sim: info: usage: $: ./cache_sim trace_file.txt [line_size argument] [coherency protocol]");
	exit (1);
      } else { line_size = val; }
    } else {
      line_size = val;
      protocol = std::string(argv[3]);
    }
  }

  //Correctness checking
  //Line-size must be a power of two between 0 and 2048;
  if ( !((line_size > 0) && (line_size <= 2048)) || ( log2(line_size) != ceil(log2(line_size)) ) ){
      ERR("sim: err: line size parameter must be a power of two between 0 and 2048");
      exit (1);
  }

  if ( (protocol.compare("MSI") != 0) && (protocol.compare("MESI") != 0) && (protocol.compare("MES") != 0) ){
    //std::string::compare returns !0 if the strings do not match
      ERR("sim: err: illegal protocol parameter");
      LOG("sim: info: legal protocols are MSI, MESI, or MES");
      exit (1);    
  }
  
  sim = new Sim(line_size, protocol); //Initialise the new simulator with set or default params
  
  file_name = argv[1]; //Grab the trace file name
  
  infile.open( file_name ); //Open the trace
  if ( infile.fail() ){
    ERR("sim: err: trace file could not be opened");
    INFO("please check that the file exists and you have permission to access it");
    exit (1);
  }
  
  LOG("sim: info: reading from trace file - " << file_name);

  //Read the trace file line by line
  while ( std::getline(infile, line) ){
    std::istringstream instream(line);
    std::string proc, op;
    int addr;
    if ( !(instream >> proc >> op >> addr) ){
      
      bool option = (proc.compare("v") == 0)
	&& (proc.compare("p") == 0)
	&& (proc.compare("h") == 0)
	&& (proc.compare("i") == 0)
	&& (proc.compare("s") == 0);
      
      
      if ( !option ){
	sim->option(proc);
	continue;
      } else {
	ERR("sim: err: incorrect optional argument given in trace file");
	LOG("sim: info: valid options are p, h, v, i");
	exit (1);
      }
    }
    
    //Proc tells me which processor to use, op tells me the operation and address is, well, the address
    sim->execute(proc, op, addr);
    
  }



  infile.close(); //Close the trace file
  delete sim;
  return 0;
}
