#ifndef SIM
#define SIM

#include "global.h"

class Sim{
public:
  //Possible constructions
  Sim(int line_size, std::string protocol); //Cache line size and coherency protocol
  Sim(int line_size); //Cache line size
  Sim(std::string protocol); //Coherency protocol
  Sim(); //Default constructor

  ~Sim(); //Destructor
  
  int execute(std::string proc, std::string op, int address); //Execute an op on a processor
  int option(std::string op); //Enable, disable, or execute an optional input
  Processor **cpu_array; //Array holding the simulation's cpus (as pointers)
  bool output = false;
private:
  int cpus = 4; //Number of Processors in the system
  int line_size = 4; //Default line size
  std::string protocol = "MSI"; //Default protocol

  Bus *bus; //The bus keeping track of the reads and writes


};


#endif
