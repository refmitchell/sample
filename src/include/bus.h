#ifndef BUS
#define BUS

#include "global.h"
class Sim;

class Bus{
public:
  Bus(Sim *parent_sim);
  int rd(int proc, address_t block);
  int rdx(int proc, address_t block);
  int upgr(int proc, address_t block);
  bool exc_check(int proc, address_t block);
  int select_server(address_t block);
  int write_update(int proc, address_t block);
  Sim *show_parent();
  
private:
  Sim *parent;
  
};

#endif
