#ifndef CPU
#define CPU

#include "global.h"

class Bus;

typedef struct{
  int tag;
  int index;
  int offset;
  int address;
} address_t;

enum state { M, E, S, I }; //Enum for the possible states of the cache

class Processor{


 public:
  Processor(int line_size, std::string protocol, Bus* sim_bus, bool const *output_flag, int id);
  Processor(); //Should never be used
  ~Processor();

  bool read(int address);
  bool write(int address);

  float get_hit_rate(); //Get the hit rate for the cache
  float get_invalidation_count(); //Get the invalidation rate for the cache
  std::string get_summary_stats(); //Compile and retrieve a final summary stat for the cache
  std::string get_cache_contents(); //Get the cache contents
 
  
 private:

  int id;
  std::string protocol;
  int *cache; //Cache representation
  state *cache_state; //Store the state for each line in the cache
  int number_of_lines; //Number of lines in the cache
  int line_size = 4; //Line size for the cache
  const bool *output;
  Bus *bus;
  //  address_t data; //Address structure broken down into tag, offset, and index
  
  address_t decomposeAddress(int address); //Break address into the tag, index, and offset
  int load(int address); //Loads may only happen on a miss.
  int off_bits; //Number of bits for the offset
  int off_mask; //Binary mask for extracting the offset
  int idx_bits; //Number of bits for the index
  int idx_mask; //Binary mask for extracting the index
  int tag_shft; //Number of bits needed to right-shift for the tag

  //Selection of functions to handle the state transitions depending on the protocol
  //Operations originating here
  int r_miss(address_t block);
  int r_hit(address_t block);
  int w_miss(address_t block);
  int w_hit(address_t block);

  //Stats tracker
  int hits = 0;
  int invalidations = 0;
  int accesses = 0;
  int mm_loads = 0; //Loads from memory
  int write_backs = 0; //Write backs to memory
  int bus_loads = 0; //Loads from bus (p2p cache transactions)
  int bus_writes = 0; //Writes to bus (p2p cache transactions)
  
  int write_updates_sent = 0; //Updates which are automatically pushed from here
  int write_updates_received = 0; //Updates which are automatically received here
  int ignored_updates = 0;

  //note: write updates and bus loads/writes are subtly different things
  //write updates are sent out by the modifying processor
  //bus operations are governed by the requesting processor
  bool res_request(address_t data); 

 public:

  //Operations originating at other CPUs (inverse bus snooping)
  int bus_rd_callback(address_t block); 
  int bus_rdx_callback(address_t block);  
  int bus_upgr_callback(address_t block);
  int bus_write_update(address_t block);
  
  bool bus_res_request(address_t block);
  
};



#endif
