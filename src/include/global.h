#ifndef GLOBAL //Header guard
#define GLOBAL

//Debug switch
#define DEBUG 1

//includes
#include <iostream> //IO
#include <string> //Strings
#include <sstream>
#include <fstream> //File streams
#include <vector> //Vector data structure
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

//local includes
#include "processor.h"
#include "sim.h"
#include "bus.h"


//definitions
#define LOG(x) std::cout << x << std::endl
#define ERR(x) std::cerr << x << std::endl
#define INFO(x) std::cout << "sim: info: " << x << std::endl

#if DEBUG
#define DBG(x) std::cout << "sim: debug: " << x << std::endl
#else
#define DBG(x)
#endif

#endif
