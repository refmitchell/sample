#ifndef SIM
#define SIM

#include "global.h"

class Simulator{
public:
  //Construction/Destruction
  Simulator(); //Default initialisation (MSI, 512 lines, 4 words per line)
  Simulator(int cache_lines, int line_size, std::string protocol); //Fully specified
  Simulator(std::string protocol); //Protocol only specified

  ~Simulator();

  //Public methods
  int execute(std::string proc, std::string op, int address); //Execute a command in the system
  int option(std::string op);
  
private:
  CPU cpu_array[];
  bool option_output = false; //Toggle switch for line-by-line info
  bool print_cache_contents();
  bool print_hit_rate();
  bool print_invalidations();

  
};


#endif
