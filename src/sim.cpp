#include "include/sim.h"

//Construction/Destruction
Sim::Sim(int line_size, std::string protocol){
  this->output = false;
  this->line_size = line_size;
  this->protocol = protocol;
  cpu_array = new Processor*[this->cpus]; //New array of Processor*
  this->bus = new Bus(this);
  for ( int i = 0; i < this->cpus; ++i ){
    //Each element is a new Processor; needs the line_size, protocol, and bus pointer
    //All should be consts in declaration 
    cpu_array[i] = new Processor(this->line_size, this->protocol, this->bus, &(this->output), i); 
  }


}

Sim::Sim(int line_size){
  this->line_size = line_size;
  Sim::output = false;
  cpu_array = new Processor*[this->cpus]; //New array of Processor*
  for ( int i = 0; i < this->cpus; ++i ){
    //Each element is a new Processor; needs the line_size, protocol, and bus pointer
    //All should be consts in declaration 
    cpu_array[i] = new Processor(this->line_size, this->protocol, this->bus, &(this->output), i); 
  }

  this->bus = new Bus(this);
}

Sim::Sim(std::string protocol){
  this->protocol = protocol;
  Sim::output = false;
  cpu_array = new Processor*[this->cpus]; //New array of Processor*
  for ( int i = 0; i < this->cpus; ++i ){
    //Each element is a new Processor; needs the line_size, protocol, and bus pointer
    //All should be consts in declaration 
    cpu_array[i] = new Processor(this->line_size, this->protocol, this->bus, &(this->output), i); 
  }
  this->bus = new Bus(this);
}

Sim::Sim(){
  Sim::output = false;
  cpu_array = new Processor*[this->cpus]; //New array of Processor*
  for ( int i = 0; i < this->cpus; ++i ){
    //Each element is a new Processor; needs the line_size, protocol, and bus pointer
    //All should be consts in declaration 
    cpu_array[i] = new Processor(this->line_size, this->protocol, this->bus, &(this->output), i); 
  }
  this->bus = new Bus(this);
}

Sim::~Sim(){
  delete cpu_array;
  delete bus;
}

//Methods
int Sim::execute(std::string proc, std::string op, int address){
  int cpu;
  //Extract the number of the Processor we're working with as an int
  sscanf(proc.c_str(), "P%d",  &cpu);

  //Do reads and writes using "atomic" functions
  //Passed in bus pointer should deal with protocol handling
  if ( op.compare("W") == 0 ){
    this->cpu_array[cpu]->write(address);
  } else if ( op.compare("R") == 0 ){
    this->cpu_array[cpu]->read(address);
  } else{
    ERR("sim: err: undefined memory operation used");
    LOG("sim: info: mem op was " << op);
    LOG("sim: info: valid ops are R or W");
    LOG("sim: exit");
    exit (1);
  }

  return 0;
}

int Sim::option(std::string op){
  if ( op.compare("p") == 0 ){
    //Print cache contents 
    //Processor->get_cache_contents_string();
    std::stringstream ss;
    ss << std::endl << "cache content:" << std::endl;
    
    for ( int i = 0; i < this->cpus; ++i ){
      ss << this->cpu_array[i]->get_cache_contents();
      ss << std::endl << "===" << std::endl;
    }

    LOG(ss.str());

  } else if ( op.compare("v") == 0 ){
    this->output = !this->output; //Toggle output
  } else if ( op.compare("h") == 0 ){
    //Print hit rate
    //Processor->get_hit_rate()
    std::stringstream ss;
    ss << std::endl << "hit rate so far: " << std::endl;
    for ( int i = 0; i < this->cpus; ++i ){
      ss << "P" << i << " " << this->cpu_array[i]->get_hit_rate() << std::endl;
      ss << std::endl;
    }
    
    LOG(ss.str());
    
  } else if ( op.compare("i") == 0 ){
    //Print invalidation rate
    //Processor->get_invalidation_rate();
    std::stringstream ss;
    ss << std::endl << "invalidation count so far: " << std::endl;
    for ( int i = 0; i < this->cpus; ++i ){
      ss << "P" << i << " " << this->cpu_array[i]->get_invalidation_count() << std::endl;
      ss << std::endl;
    }
    
    LOG(ss.str());

  } else if (op.compare("s") == 0){
    //Print summary statistics for all processors
    DBG(this->cpu_array[0]->get_summary_stats());
    DBG(this->cpu_array[1]->get_summary_stats());
    DBG(this->cpu_array[2]->get_summary_stats());
    DBG(this->cpu_array[3]->get_summary_stats());
          

  } else {
    ERR("sim: err: undefined trace option used");
    LOG("sim: info: option was " << op);
    LOG("sim: info: valid ops are p, v, h, i, or s");
    LOG("sim: exit");
    exit (1);
  }

  return 0;
}
