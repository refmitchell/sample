#include "include/global.h"
#include "include/processor.h"
#include <bitset> //DEBUG
//Construction/Destruction
Processor::Processor(int line_size, std::string protocol, Bus* sim_bus,
		     bool const *output_flag, int id){
  this->id = id;
  this->line_size = line_size;
  this->protocol = protocol;
  this->output = output_flag;
  this->bus = sim_bus;
  this->number_of_lines = 2048 / line_size;
  cache = new int[this->number_of_lines];
  this->cache_state = new state[this->number_of_lines];

  for ( int i = 0; i < this->number_of_lines; ++i ){
    cache[i] = -1;
    this->cache_state[i] = I; 
  }


  //Set up masking and bitshifting parameters for the cache indexing process;
  //Assume 32 bits (ints are 32 bits in cpp anyway)
  this->off_bits = (int) ceil(log2(this->line_size));
  this->idx_bits = (int) ceil(log2(this->number_of_lines));
  this->tag_shft = this->idx_bits + this->off_bits;
  this->off_mask = pow(2, this->off_bits) - 1; //Say we want 4 LSB, do 2^4 - 1 = 15 = bx1111
  this->idx_mask = ((int) pow(2, (this->tag_shft)) - 1);
  this->idx_mask = this->idx_mask & (~(this->off_mask));

 
  /*
  
  int tag = address >> (offset_bits + index_bits); //Right shift to lose all the bits not in the tag
  int offset = address & offset_mask; //Bitwise AND with the mask to get just those bits
  
  //Bitwise AND with the mask to get just those bits; needs shifted to get the proper int representation
  int index = (address & index_mask) >> offset_bits;

  DBG("adr: " << std::bitset<32>(address) ) ;
  DBG("tag: " << std::bitset<32>(tag) );
  INFO("idx: " << std::bitset<32>(index) );
  INFO("off: " << std::bitset<32>(offset) );
  //Assume the cache size is a power of two; if it's not issues of indexing occur.

  address_t data = { tag, index, offset, address }; //Construct

  INFO("address: " << data.address << " tag: " << data.tag << " index: " << data.index << " offset: " <<
  data.offset);*/
  
}

Processor::Processor(){

}

Processor::~Processor(){
  delete cache;
  delete this->cache_state;
}

bool Processor::read(int address){ //Read request
  //For future Rob: This still works in the case of MES; so long as the block
  //is in the cache it's considered a hit; the message broadcast on the bus
  //then updates the blocks in the other caches (i.e. a write updates all caches)
  //Blocks in MES can never be invalid so the last if condition still works
  address_t data = this->decomposeAddress(address); //Get tag, index, and offset

  ++accesses;
  
  if ( *(this->output) ){
    INFO("p" << this->id << " performing a read; looking for tag " <<
	 data.tag << " at index " << data.index);
  }
  
  if ( (cache[data.index] == data.tag) && (this->cache_state[data.index] != I) ){
    if ( *(this->output) ){
      INFO("cache hit; found tag " << data.tag << " at index " << data.index );
    }
    ++hits;
    this->r_hit(data);
    return true;
  }  //Check for tag at index

  if ( *(this->output) ){
    INFO("cache miss; calling handling callback");
  }

  //Modified data which is stored here requires a write back
  if ( this->cache_state[data.index] == M ){
    ++(this->write_backs); //Write the block back

    if ( this->protocol.compare("MES") != 0 ){
      this->cache_state[data.index] = I; //Invalidate the current copy
      ++(this->invalidations);
    }
    
  } 
  
  this->r_miss(data);
 
  cache[data.index] = data.tag; //This should be fine, but worth keeping an eye on
  return false;
}

bool Processor::write(int address){ //Write request
  //For future Rob: This still works in the case of MES; so long as the block
  //is in the cache it's considered a hit; the message broadcast on the bus
  //then updates the blocks in the other caches (i.e. a write updates all caches)
  //Blocks in MES can never be invalid so the last if condition still works
  address_t data = this->decomposeAddress(address); //Get tag, index, and offset
  ++accesses;

  if ( *(this->output) ){
    INFO("p" << this->id << " performing a write; looking for tag " <<
	 data.tag << " at index " << data.index);
  }
  
  if ( (cache[data.index] == data.tag) && (this->cache_state[data.index] != I) ){
    if ( *(this->output) ){
      INFO("cache write hit; found tag " << data.tag << " at index " << data.index );
    }
    ++hits;
    this->w_hit(data); return true;
  } //Check for tag at index

  if ( *(this->output) ){
    INFO("cache write miss; calling handling callback");
  }

  //Modified data which is stored here requires a write back
  if ( this->cache_state[data.index] == M ){
    ++(this->write_backs); //Write the block back

    if ( this->protocol.compare("MES") != 0 ){
      this->cache_state[data.index] = I; //Invalidate the current copy
      ++(this->invalidations);
    }
    
    
  }
  
  this->w_miss(data);
  
  cache[data.index] = data.tag;
  return false;
}

int Processor::r_hit(address_t block){
  int index = block.index;
  state current = this->cache_state[block.index];
  //No operation is required on a read hit for all protocols;
  //Simply output logging info 
  if ( protocol.compare("MSI") == 0 ){
    //State is not changed as block must be either M or S
    if ( *(this->output) ){ INFO("msi: p" << this->id <<": read hit, nothing to do"); }
  } else if ( protocol.compare("MESI") == 0 ){
    //Do nothing; on a read hit, MESI state stays the same and no broadcasts are made
    if ( *(this->output) ){ INFO("mesi: p" << this->id << ": read hit, nothing to do"); }
  } else if ( protocol.compare("MES") == 0 ){
    //Do nothing; on a read hit, MESI state stays the same and no broadcasts are made
    if ( *(this->output) ){ INFO("mes: p" << this->id << ": read hit, nothing to do"); }    
  } else {
    ERR("sim: err: unsupported coherency protocol used " << protocol);
    LOG("sim: info: supported protocols are MSI, MESI, and MSI");
  }

  return 0; 
}

int Processor::r_miss(address_t block){
  int index = block.index;
  state current = this->cache_state[index];
  if ( protocol.compare("MSI") == 0 ){
    //Either block was invalid and in the cache or it was just not in the cache
    //and the existing block was written back and/or invalidated
    //In either case, the block is now shared
    
    this->cache_state[index] = S; //New state
    if ( *(this->output) ){ INFO("msi: p" << this->id <<": loading " << block.address << " for read"); }
    this->bus->rd(this->id, block); //Broadcast read request at this processor
    ++(this->mm_loads); //Read miss requires a load from main memory
  } else if ( protocol.compare("MESI") == 0 ){
    
    //Cache miss; block was either invalid or needs read from memory
    this->bus->rd(this->id, block); //Broadcast bus read
    
    bool exclusive = this->bus->exc_check(this->id, block); //Check the exclusivity of the block
    if ( exclusive ){
      this->cache_state[block.index] = E;
      if ( *(this->output) ){ INFO("mesi: p" << this->id << ": " << block.address <<
				   " loaded as exclusive"); }
      ++(this->mm_loads); //No other cache has the block so load from mm
    } else {
      this->cache_state[block.index] = S;
      if ( *(this->output) ){ INFO("mesi: p" << this->id << ": " << block.address <<
				   " loaded as shared"); }
      ++(this->bus_loads);
    } //Else shared

  } else if ( protocol.compare("MES") == 0 ){
    //Cache miss; block was either invalid or needs read from memory
    this->bus->rd(this->id, block); //Broadcast bus read
    
    bool exclusive = this->bus->exc_check(this->id, block); //Check the exclusivity of the block
    if ( exclusive ){
      this->cache_state[block.index] = E;
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": " << block.address <<
				   " loaded as exclusive"); }
      ++(this->mm_loads); //No other cache has the block so load from mm
    } else {
      this->cache_state[block.index] = S;
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": " << block.address <<
				   " loaded as shared"); }
      ++(this->bus_loads);
    } //Else shared
  } else {
    ERR("sim: err: unsupported coherency protocol used " << protocol);
    LOG("sim: info: supported protocols are MSI, MESI, and MSI");
  }
  return 0; 
}


int Processor::w_hit(address_t block){
  int index = block.index;
  state current = this->cache_state[index];
  if ( protocol.compare("MSI") == 0 ){
    
    this->cache_state[index] = M; //Cache state either goes from S > M or stays as M
    if ( *(this->output) ){ INFO("msi: p" << this->id <<": block " << block.address << " modified"); } 
      
    this->bus->upgr(this->id, block); //Broadcast bus upgrade message
    
  } else if ( protocol.compare("MESI") == 0 ){

    if ( this->cache_state[block.index] == S ){
      this->bus->upgr(this->id, block);
      if ( *(this->output) ){ INFO("mesi: p" << this->id << ": bus_upgr broadcast for "
				   << block.address); }
    } // Broadcast bus upgrade
    this->cache_state[index] = M; //Change state to M regardless of exclusivity 
  } else if ( protocol.compare("MES") == 0 ){
    if ( this->cache_state[block.index] == S ){
      this->bus->write_update(this->id, block);
      ++(this->write_updates_sent); //Block remains shared in this case as we've broadcast and update
      ++(this->write_backs); //The updated data must also be written back to memory
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": write update broadcast for "
				   << block.address); }
    } else {
            if ( *(this->output) ){ INFO("mes: p" << this->id << ": silent write update broadcast for "
				   << block.address); }
      this->cache_state[block.index] = M; //Block is exclusive and modified
    }
  } else {
    ERR("sim: err: unsupported coherency protocol used " << protocol);
    LOG("sim: info: supported protocols are MSI, MESI, and MSI");
  }

  return 0; 
}
  
int Processor::w_miss(address_t block){
  int index = block.index;
  state current = this->cache_state[index];
  if ( protocol.compare("MSI") == 0 ){

    if ( *(this->output) ){ INFO("msi: p" << this->id << ": loading " << block.address << " for write"); }
    
    this->bus->rdx(this->id, block); //Broadcast readx request at this processor
    this->cache_state[index] = M; //After a write miss the state is always modified
    ++(this->mm_loads); //Write here requires load from MM
    
  } else if ( protocol.compare("MESI") == 0 ){
    bool exclusive = this->bus->exc_check(this->id, block);
    //Check needs to happen here, out of the correct order
    //If we check after rdx is broadcast the block will always
    //be exclusive
    if ( exclusive ){ //Load from memory
      ++(this->mm_loads);
    } else { //Grab from peer cache
      ++(this->bus_loads);
    }
    
    if ( *(this->output) ){ INFO("mesi: p" << this->id << ": loading " << block.address << " for write"); }
    if ( *(this->output) ){ INFO("mesi: p" << this->id << ": broadcasting bus_rdx for " << block.address); }
    
    this->bus->rdx(this->id, block);
    this->cache_state[block.index] = M;
  } else if ( protocol.compare("MES") == 0 ){
    bool exclusive = this->bus->exc_check(this->id, block);

    if ( *(this->output) ){ INFO("mes: p" << this->id << ": loading " << block.address << " for write"); }
    
    if ( exclusive ){
      ++(this->mm_loads);
      this->cache_state[block.index] = M;      
    } else {
      this->bus->rdx(this->id, block); //Broadcast write miss to other caches
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": broadcasting bus_rdx for " << block.address); }
      ++(this->bus_loads);
      this->cache_state[block.index] = S; //Set state to shared
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": broadcasting write update for "
				   << block.address); } 
      this->bus->write_update(this->id, block);
      ++(this->write_backs);
      ++(this->write_updates_sent);
    }
    
  } else {
    ERR("sim: err: unsupported coherency protocol used " << protocol);
    LOG("sim: info: supported protocols are MSI, MESI, and MSI");
  }

  return 0; 
}

//Operating a kind of inverted snooper; the bus instead informs all processors
//of a certain broadcast
int Processor::bus_rd_callback(address_t block){ //Called on a busrd
  int index = block.index;
  bool in_cache = this->res_request(block); //Is this block in our cache?
  if ( protocol.compare("MSI") == 0 ){

    //Block flushed onto the bus and state goes to shared
    //Data is written back to MM
    if ( *(this->output) ){ INFO("msi: p" << this->id << ": bus_rd received for " << block.address ); } 
    if ( in_cache && (this->cache_state[index] == M) ){ //If we have this block and it's dirty
      if ( *(this->output) ){ INFO("msi: p" << this->id << ": " << block.address << " found; writing to memory" ); } 
      this->cache_state[index] = S; ++(this->write_backs);
    }

    
  } else if ( protocol.compare("MESI") == 0 ){
    if ( (this->cache_state[block.index] != I) && in_cache ){
      //then we need to do something
      //If exclusive +1 bus_write and state transition
      //If shared +1 bus_write and state transition (*)
      //If modified +1 write_back; +1 bus_write; and state transition
      //(*) only one processor must supply the write in the shared case
      if ( (this->cache_state[block.index] == E) ){
	++(this->bus_writes);
      } else if ( this->cache_state[block.index] == M ){ //Implies exculsivity
	++(this->write_backs); ++(this->bus_writes);
	if ( *(this->output) ){	INFO("mesi: p" << this->id << ": shared data for  "
				     << block.address);	}
      } else if ( this->cache_state[block.index] == S){
	int server = this->bus->select_server(block);
	if ( server == this->id ){
	  ++(this->bus_writes);
	  if ( *(this->output) ){ INFO("mesi: p" << this->id << ": shared data for  "
				       << block.address);  }
	}
      } else {
	ERR("sim: err: illegal state encountered");
	exit (1);
      }

      if ( *(this->output) ){
	INFO("mesi: p" << this->id << ": bus_rd received for " << block.address);
	INFO("mesi: p" << this->id << ": block " << block.address << " now shared");
      }
      
      this->cache_state[block.index] = S;  //Block is now being shared
    }
  } else if ( protocol.compare("MES") == 0 ){
    bool in_cache = this->res_request(block);
    
    if ( (this->cache_state[block.index] == E) && in_cache){
      ++(this->bus_writes);
    } else if ( (this->cache_state[block.index] == M ) && in_cache ){ //Implies exculsivity
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": shared data for  "
				   << block.address);}
      ++(this->write_backs); ++(this->bus_writes);
    } else if ( this->cache_state[block.index] == S && in_cache ){
      int server = this->bus->select_server(block);
      if ( server == this->id ){
	if ( *(this->output) ){
	  INFO("mes: p" << this->id << ": shared data for  " << block.address);
	}	  
	++(this->bus_writes);
      }
    } 

    if ( *(this->output) ){
      INFO("mes: p" << this->id << ": bus_rd received for " << block.address);
      INFO("mes: p" << this->id << ": block " << block.address << " now shared");
    }
      
    if ( !(this->cache_state[block.index] == I) && in_cache){ //If the block is not cold and is in the cache
      //If it's not in the cache then we're incorrectly modifying the state of the block
      this->cache_state[block.index] = S;  //Block is now being shared
    }
  } else {
    ERR("sim: err: unsupported coherency protocol used " << protocol);
    LOG("sim: info: supported protocols are MSI, MESI, and MSI");
  }  
}

int Processor::bus_rdx_callback(address_t block){ //Called on a busrdx
  int index = block.index;
  state current = this->cache_state[index];
  if ( protocol.compare("MSI") == 0 ){
    bool in_cache = this->res_request(block);
    if ( *(this->output) ){ INFO("msi: p" << this->id << ": bus_rdx received for " << block.address ); } 
    if ( in_cache && (this->cache_state[block.index] == S) ){
      this->cache_state[index] = I;
      if ( *(this->output) ){ INFO("msi: p" << this->id << ": " << block.address << " found; invalidating" ); }
      ++(this->invalidations);
    } else if ( in_cache && (this->cache_state[block.index] == M) ){
      this->cache_state[index] = I;
      if ( *(this->output) ){ INFO("msi: p" << this->id << ": " << block.address << " found; writing to memory and invalidating" ); }
      ++(this->invalidations);
      ++(this->write_backs);
      //Data needs to be written back "before" it can be read for the requested write
    }
    
  } else if ( protocol.compare("MESI") == 0 ){
    //If the block is in the cache and this is received then we invalidate.
    if ( this->res_request(block) && (this->cache_state[block.index] != I) ){
      if ( *(this->output) ){ INFO("mesi: p" << this->id << ": bus_rdx received for " << block.address ); } 
      if ( (this->cache_state[block.index] == E) ){
	++(this->bus_writes);
      } else if ( this->cache_state[block.index] == M ){ //Implies exculsivity
        ++(this->bus_writes); ++(this->write_backs); //This time do not write to MM
      } else if ( this->cache_state[block.index] == S){
	int server = this->bus->select_server(block);
	if ( server == this->id ){
	  if ( *(this->output) ){ INFO("mesi: p" << this->id << ": data shared for " << block.address ); } 
	  ++(this->bus_writes);
	}
      } else {
	ERR("sim: err: illegal state encountered");
	exit (1);
      }

      if ( *(this->output) ){ INFO("mesi: p" << this->id << ": " << block.address << " now invalid"); }       
      this->cache_state[block.index] = I; ++(this->invalidations);
    } 
  } else if ( protocol.compare("MES") == 0 ){
    if ( this->res_request(block) ){ //IF the block's in this cache
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": bus_rdx received for " << block.address ); } 

      if ( (this->cache_state[block.index] == E) ){
	++(this->bus_writes);
      } else if ( this->cache_state[block.index] == M ){ //Implies exculsivity
	++(this->bus_writes); ++(this->write_backs); //This time to not write to MM
      } else if ( this->cache_state[block.index] == S){
	int server = this->bus->select_server(block);
	if ( server == this->id ){
	  ++(this->bus_writes);
	  if ( *(this->output) ){ INFO("mesi: p" << this->id << ": data shared for " << block.address ); } 

	}
      } else {
	ERR("sim: err: in rd_callback");
	ERR("sim: err: illegal state encountered");
	exit (1);
      }
      //If the block was in the cache update its state
      if ( *(this->output) ){ INFO("mes: p" << this->id << ": " << block.address << " now shared"); }       
      this->cache_state[block.index] = S; //Block is now shared
    }


  } else {
    ERR("sim: err: unsupported coherency protocol used " << protocol);
    LOG("sim: info: supported protocols are MSI, MESI, and MSI");
  }

  return 0; 
}

int Processor::bus_upgr_callback(address_t block){ //Called on a busupgr
  int index = block.index;
  state current = this->cache_state[index];
  bool in_cache = this->res_request(block);
  if ( protocol.compare("MSI") == 0 ){

    if ( *(this->output) ){ INFO("msi: p" << this->id << ": bus_upgr received for " << block.address ); }
    if ( this->res_request(block) && (this->cache_state[index] == S) ){
      if ( *(this->output) ){ INFO("msi: p" << this->id << ": " << block.address << "found; invalidating"); }
      this->cache_state[index] = I;
      ++(this->invalidations);
    } else if ( in_cache && this->cache_state[index] == M ){
      //Included for validation purposes
      LOG(this->get_cache_contents());
      ERR("sim: err: impossible state reached");
      LOG("sim: info: block in state M received bus_upgr broadcast");
      INFO("suspect address " << block.address );
      exit (1);
    }
    
  } else if ( protocol.compare("MESI") == 0 ){
    //If the block is in the cache and this is received then we invalidate.
    //This should only be received in the shared state
    if ( this->res_request(block) && ( this->cache_state[block.index] == S) ){
      if ( *(this->output) ){ INFO("mesi: p" << this->id << ": " << block.address
				   << " found; invalidating"); }
      this->cache_state[block.index] = I; ++(this->invalidations);
    } else if ( this->res_request(block) && (this->cache_state[block.index] != I) ){
      //Included for validation purposes
      LOG(this->get_cache_contents());
      ERR("sim: err: impossible state reached");
      LOG("sim: info: block in state exclusive state received bus_upgr broadcast");
      INFO("suspect address " << block.address );
      INFO("at index " << block.index );
      exit (1);      
    }
  } else if ( protocol.compare("MES") == 0 ){
    //This message is never sent in MES; we leave this in for completeness
  } else {
    ERR("sim: err: unsupported coherency protocol used " << protocol);
    LOG("sim: info: supported protocols are MSI, MESI, and MSI");
  }

  return 0; 
}


int Processor::bus_write_update(address_t block){
  //Block updated remains shared
  //This method is only called if the block is present in this cache
  if ( !(this->res_request(block)) ){
    if ( *(this->output) ){ INFO("mes: p" << this->id << ": write update received for " << block.address << " which was not present"); }
    ++(this->ignored_updates);
    return 1;
  }
  
  if ( *(this->output) ){ INFO("mes: p" << this->id << ": write update received for " << block.address); }
  ++(this->write_updates_received);
  return 0;
}


//Methods
//Compose a struct containing the address and the tag of the data.
address_t Processor::decomposeAddress(int address){
  //Assume 32 bits (ints are 32 bits in cpp anyway so this is convenient)
  int tag = address >> this->tag_shft;
  int offset = address & this->off_mask;
  int index = (address & this->idx_mask) >> this->off_bits;
  address_t data = { tag, index, offset, address };
  /* // DEBUG
  DBG("idx: " << index);
  DBG("adr: " << std::bitset<32>(address) ) ;
  DBG("tag: " << std::bitset<32>(tag) );
  DBG("idx: " << std::bitset<32>(index) );
  DBG("off: " << std::bitset<32>(offset) );*/
  
  return data;
}

bool Processor::bus_res_request(address_t block){ return (this->res_request(block)); }

bool Processor::res_request(address_t data){
  if ( (cache[data.index] == data.tag) && (this->cache_state[data.index] != I) ){
    return true;
  }
  return false;
}

float Processor::get_invalidation_count(){ return (this->invalidations); }

float Processor::get_hit_rate(){ return ((float) hits / (float) accesses); }

std::string Processor::get_cache_contents(){
  //For the processor want:
  //PX
  //<linum> <tag> <state>
  //<linum> <tag> <state>
  // and so on...

  std::stringstream ss;
  ss << "P" << this->id << std::endl;

  for ( int i = 0; i < this->number_of_lines; ++i ){
    char state = ' ';
    if ( this->cache_state[i] == M ){ state = 'M'; }
    else if (this->cache_state[i] == E) { state = 'E'; }
    else if (this->cache_state[i] == S) { state = 'S'; }
    else if (this->cache_state[i] == I) { state = 'I'; }
    ss << i << " " << this->cache[i] << " " << state << std::endl;
  }

  //  DBG(ss.str());
  return ss.str();
}

//Create a summary statistic string for this processor
std::string Processor::get_summary_stats(){
  std::stringstream ss;
  
  float invalidations = this->get_invalidation_count();
  float hit_rate = this->get_hit_rate();
  float miss_rate = 1 - hit_rate;
  int mm_loads = this->mm_loads; //Loads from main memory
  int mm_writes = this->write_backs;  //Writes to main memory
  int bus_loads = this->bus_loads; //Loads from the bus
  int bus_writes = this->bus_writes; //Writes/flushes to the bus
  int write_updates_sent = this->write_updates_sent;
  int write_updates_received = this->write_updates_received;
  int write_updates_ignored = this->ignored_updates;

  ss << std::endl << std::endl;
  ss << "Summary statistics for P" << this->id << ": " << std::endl;
  ss << "configuration: protocol: " << this->protocol << "; line size: " << this->line_size
     << "; number of lines: " << this->number_of_lines << ";" << std::endl;
  
  ss << "invalidations: " << invalidations << std::endl;
  ss << "hit rate: " << hit_rate << std::endl;
  ss << "miss rate: " << miss_rate << std::endl;
  ss << "loads from main memory: " << mm_loads << std::endl;
  ss << "write backs to main memory: " <<  mm_writes << std::endl;
  ss << "p2p writes: " << bus_writes << std::endl;
  ss << "p2p reads: " << bus_loads << std::endl;
  ss << "write updates sent: " << write_updates_sent << std::endl;
  ss << "write updates received: " << write_updates_received << std::endl;
  ss << "write updates ignored: " << write_updates_ignored << std::endl;
  ss << std::endl;

  return ss.str();
}

